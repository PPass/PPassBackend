## Create some middleware for catching mongoDB errors

## Few bugs are marked with "#note"

## Winston is disabled for now. Will enable it as soon as I'll find any reason to log user's actions

## Clean interfaces. Some interfaces are duplicated

## Change generic middleware params? IDE says that params are wrong

## Add better email details ( config details )

## Rewrite registration email string and add better code because current code is written just to work

## New error validator still breaks some functions. Especially util functions

## Add cleanup function to start whenever user break request or request time out

## Some errors are throwing strange errors. put user/data is throwing id error with email template

## Add 404 error messages when user send wrong type of data

## Validation for when user want to edit his credentials

## If I am not wrong, validation url token if jwt token for now. Fix it