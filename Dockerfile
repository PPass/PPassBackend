FROM node:16

WORKDIR /usr/src/ppass
COPY package.json yarn.lock ./
RUN yarn install --frozen-lockfile
COPY /build ./build

RUN yarn install --production
EXPOSE 3006
USER node
CMD ["npm", "start"]
