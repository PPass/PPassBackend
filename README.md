# This project is backend for password manager.

## How to use

App by default will look for config files in `config` folder, which should be located in root directory. There are 2
config files:

- devConfig.json
- prodConfig.json

Each file should contain data like:

```
{
  "mongoUrl": "MongoDb url string",
  "jwt": "json web token string"
  "jwt": "json web token refresh"
}
```

If you want to work on this app, use "npm dev" or "yarn dev". This function will use devConfig.json file. "npm start"
or "yarn start" will use prodConfig.json file.
