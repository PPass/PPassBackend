import jwt from 'jsonwebtoken';
import getConfig from '../tools/configLoader';
import { tokens } from './types';
import { Response } from 'express';
import { JWTData } from '../types';

export const validateToken = (cookies: tokens, res: Response): void => {
  const { access, refresh } = cookies;
  const config = getConfig();

  if (refresh) {
    const payload = jwt.verify(refresh, config.refreshToken) as JWTData;
    if (payload) res.locals.user = payload.id;
    return;
  }

  if (access) {
    const payload = jwt.verify(access, config.accessToken) as JWTData;
    if (payload) res.locals.user = payload.id;
    return;
  }
};
