import devConfig from '../../config/devConfig.json';
import prodConfig from '../../config/prodConfig.json';
import { NoConfigFile } from '../errorsHandling/errors';
import { ConfigInterface } from '../types';

export default function getConfig(): ConfigInterface {
  switch (process.env.NODE_ENV) {
    case 'dev':
      return devConfig;
    case 'prod':
      return prodConfig;
    default:
      throw new NoConfigFile();
  }
}
