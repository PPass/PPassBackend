import getConfig from './configLoader';
import mongoose, { ConnectOptions } from 'mongoose';

export const mongoDB = async (): Promise<void> => {
  const config = getConfig();

  await mongoose.connect(config.mongoUrl, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  } as ConnectOptions);
};
