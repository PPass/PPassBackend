import { ErrorRequestHandler, NextFunction, Request, Response } from 'express';
import { errLogger } from '../logger';

export const errorHandler: ErrorRequestHandler = (err: Error, _req: Request, res: Response, next: NextFunction) => {
  errLogger.error(err.name + err.stack + (res.locals.id != undefined ? 'Started by: ' + res.locals.id : null));
  res.status(400).send({ message: err.message, name: err.name });
  next();
};
