export class TokenInvalid extends Error {
  constructor() {
    super('TokenInvalid');
    this.message = 'Token is invalid';
    this.name = 'TokenInvalid';
  }
}

export class NoConfigFile extends Error {
  constructor() {
    super('NoConfigFile');
    this.message = 'Config file does not exist';
    this.name = 'NoConfigFile';
  }
}

export class AlreadyLoggedIn extends Error {
  constructor() {
    super('AlreadyLoggedIn');
    this.message = 'User already logged in';
    this.name = 'AlreadyLoggedIn';
  }
}
