export interface UserData {
  id: string;
}

export interface ConfigInterface {
  mongoUrl: string;
  accessToken: string;
  refreshToken: string;
}

export interface JWTData {
  id: string;
}
