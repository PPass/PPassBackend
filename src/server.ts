import express from 'express';
import http from 'http';
import { router } from './router';
import { genericMiddleware } from './middleware/genericMiddleware';
import { mongoDB } from './tools/mongodb';
import { errorHandler } from './errorsHandling/handler';

const app = express();

app.use(genericMiddleware);
app.use(router);
app.use(errorHandler);

try {
  mongoDB().then(() => {
    http.createServer(app).listen(4010, () => {
      console.log('Connected. Listening on http 4010');
    });
  });
} catch (error) {
  console.log(error);
}
