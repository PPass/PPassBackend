import { Router } from 'express';
import { router as userRouter } from './modules/user/router';
import { router as passwordsRouter } from './modules/passwords/router';
import { warnLogger } from './logger';

export const router = Router();

router.use('/user', userRouter);
router.use('/password', passwordsRouter);

// 404
router.all('/*', (req, res) => {
  let body = '';
  Object.keys(req.body).forEach((key) => {
    body += `${key}, `;
  });
  warnLogger.warn(
    `Requested url: ${req.url} was requested by ${req.ip}, with method ${req.method} and keys in body: ${body}`,
  );
  res.status(404).json({ message: `Cannot ${req.method} ${req.url}`, error: 'Not Found' });
});
