import bcrypt from 'bcrypt';
import PasswordValidator from 'password-validator';
import { UserLoginInterface, UsersInterface } from './types';
import * as errors from './errors';

export const userDataValidation = (userProperties: UsersInterface): void => {
  genericValidator(userProperties);
  validatePassword(userProperties.password, userProperties.password2);
};

const genericValidator = (userProperties: UsersInterface): void => {
  if (!userProperties) throw new errors.NoUserData();
};

export const validatePreLogin = (data: UserLoginInterface): void => {
  if (!data.username || !data.password) throw new errors.LoginError();
};

// Validate password
const validatePassword = (password: string, password2: string | undefined): void => {
  if (password === undefined || password2 === undefined) {
    throw new errors.RegisterError(errors.registerErrors.passwordNotProvidedError);
  }
  if (password !== password2) throw new errors.RegisterError(errors.registerErrors.passwordsNotSameError);

  const schema = new PasswordValidator();
  schema.is().min(6).is().max(100).has().uppercase().has().lowercase().has().digits(1).has().not().spaces();
  if (!schema.validate(password)) throw new errors.RegisterError(errors.registerErrors.passwordNotCorrectError);
};

export const validateLoginPassword = async (password: string, hashedPassword: string): Promise<void> => {
  const auth = await bcrypt.compare(password, hashedPassword);
  if (!auth) throw new errors.RegisterError(errors.registerErrors.passwordIncorrectError);
};
