import * as mongoose from 'mongoose';
import { IUser } from './types';

const UserSchema = new mongoose.Schema(
  {
    username: {
      type: String,
      required: [true, 'Please provide valid username'],
      unique: true,
    },
    password: {
      type: String,
      required: [true, 'Please provide valid password'],
      minlength: [6, 'Min password length is 6 characters'],
      maxlength: [100, 'Max password length is 100 characters'],
    },
  },
  { timestamps: false },
);

export const User = mongoose.model<IUser>('users', UserSchema);
