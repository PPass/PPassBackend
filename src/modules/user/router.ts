import { Router } from 'express';
import { Controller } from './controller';
import { UserData } from '../../types';
import { createTokens, removeTokens } from './utils';

export const router = Router();

router.post('/login', (req, res, next) => {
  const user = new Controller();

  user
    .login(req.body, res.locals as UserData)
    .then((id: string) => createTokens(res, id))
    .catch((err) => next(err));
});

router.post('/register', (req, res, next) => {
  const user = new Controller();

  user
    .create(req.body, res.locals as UserData)
    .then(() => res.status(200))
    .catch((err) => next(err));
});

router.delete('/remove', (req, res, next) => {
  const user = new Controller();

  user
    .remove(req.body, res.locals as UserData)
    .then(() => {
      removeTokens(res);
      res.status(200);
    })
    .catch((err) => next(err));
});
