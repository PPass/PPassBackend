import bcrypt from 'bcrypt';
import { Response } from 'express';
import jwt from 'jsonwebtoken';
import * as enums from './enums';
import getConfig from '../../tools/configLoader';

export const createSalt = async (password: string): Promise<string> => {
  const salt = await bcrypt.genSalt();
  return await bcrypt.hash(password, salt);
};

export const createRandomString = (length: number): string => {
  const data = 'abcdefghijklmnouprstuwzxv1234567890(_[{]}<>';
  let string = '';
  for (let x = 0; x < length; x++) {
    string += data.at(Math.random() * 43);
  }
  return string;
};

export const createTokens = (res: Response, id: string): void => {
  const config = getConfig();

  const accessToken = jwt.sign({ id: id }, config.accessToken, { expiresIn: enums.JwtData.tokenMaxAge });
  const refreshToken = jwt.sign({ id: id }, config.refreshToken, {
    expiresIn: enums.JwtData.refreshTokenMaxAge,
  });
  res.cookie('accessToken', accessToken, { httpOnly: true, maxAge: enums.JwtData.tokenMaxAge * 1000 });
  res.cookie('refreshToken', refreshToken, { httpOnly: true, maxAge: enums.JwtData.refreshTokenMaxAge * 1000 });
  res.status(200);
};

export const refreshToken = (res: Response, id: string): void => {
  const config = getConfig();

  const refreshToken = jwt.sign({ id: id }, config.refreshToken, {
    expiresIn: enums.JwtData.refreshTokenMaxAge,
  });

  res.cookie('refreshToken', refreshToken, { httpOnly: true, maxAge: enums.JwtData.refreshTokenMaxAge * 1000 });
};

export const removeTokens = (res: Response): void => {
  res.cookie('accessToken', 'removingToken', { httpOnly: true, maxAge: 0 });
  res.cookie('refreshToken', 'removingToken', { httpOnly: true, maxAge: 0 });
};
