import { IUser, UserLoginInterface, UsersInterface } from './types';
import { User } from './model';
import { createRandomString, createSalt } from './utils';
import * as validate from './validator';
import { EmailError, emailErrors } from './errors';
import { UserData } from '../../types';
import { Rooster } from './rooster';
import { AlreadyLoggedIn, TokenInvalid } from '../../errorsHandling/errors';

export class Controller {
  rooster = new Rooster();

  async login(user: UserLoginInterface, locals: UserData): Promise<string> {
    if (locals.id) throw new AlreadyLoggedIn();
    validate.validatePreLogin(user);

    const userData = await this.rooster.findOneByType({ username: user.username });
    if (!userData) throw new EmailError(emailErrors.emailNotRegisteredError);
    await validate.validateLoginPassword(user.password, userData.password);

    return userData._id;
  }

  async create(userProperties: UsersInterface, locals: UserData): Promise<void> {
    if (locals.id) throw new AlreadyLoggedIn();

    const userData = await this.rooster.findOneByType({ username: userProperties.username });
    if (userData != undefined) throw new EmailError(emailErrors.emailAlreadyExistsError);

    validate.userDataValidation(userProperties);
    const user = userProperties;
    user.password = await createSalt(userProperties.password);
    const data = createRandomString(75);

    const newUser: IUser = new User({ ...user, validationToken: data });
    userProperties.password = await createSalt(user.password);
    return this.rooster.saveUser(newUser);
  }

  async remove(userProperties: UserLoginInterface, locals: UserData): Promise<void> {
    if (!locals.id) throw new TokenInvalid();

    const userData = await this.rooster.findOneById(locals.id);
    if (!userData) throw new EmailError(emailErrors.emailNotRegisteredError);
    await validate.validateLoginPassword(userProperties.password, userData.password);

    return this.rooster.delete(userData._id);
  }
}
