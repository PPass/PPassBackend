export enum emailErrors {
  emailAlreadyExistsError = 'Email is already registered',
  emailNotRegisteredError = 'Email is not registered',
}

export enum registerErrors {
  passwordIncorrectError = 'Password is incorrect',
  passwordNotProvidedError = 'Password or repeated password does not exist',
  passwordNotCorrectError = 'Password is not correct',
  passwordsNotSameError = 'Passwords are not the same',
}

export class EmailError extends Error {
  constructor(message: emailErrors) {
    super('EmailError');
    this.message = message;
    this.name = 'EmailError';
  }
}

export class RegisterError extends Error {
  constructor(message: registerErrors) {
    super('RegisterError');
    this.message = message;
    this.name = 'RegisterError';
  }
}

export class LoginError extends Error {
  constructor() {
    super('LoginError');
    this.message = 'Password or username not provided';
    this.name = 'LoginError';
  }
}

export class NoUserData extends Error {
  constructor() {
    super('NoUserData');
    this.message = 'User data was not provided';
    this.name = 'NoUserData';
  }
}
