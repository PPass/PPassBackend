export enum JwtData {
  tokenMaxAge = 7 * 24 * 60 * 60,
  refreshTokenMaxAge = 15 * 10,
}
