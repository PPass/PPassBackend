import { IUser, UserQueryInterface, UsersInterface } from './types';
import { User } from './model';

export class Rooster {
  async findOneById(id: string): Promise<UsersInterface | null> {
    return User.findOne({ _id: id });
  }

  async findOneByType(query: UserQueryInterface): Promise<UsersInterface | null> {
    return User.findOne({ ...query });
  }

  async saveUser(newUser: IUser): Promise<void> {
    await newUser.save();
  }

  async delete(id: string): Promise<void> {
    await User.findOneAndRemove({ _id: id });
  }
}
