import mongoose from 'mongoose';

export interface IUser extends mongoose.Document {
  _id: string;
  username: string;
  password: string;
}

export interface UsersInterface {
  _id: string;
  username: string;
  password: string;
  password2?: string;
}

export interface UserQueryInterface {
  _id?: string;
  username?: string;
}

export interface UserLoginInterface {
  username: string;
  password: string;
}
