import * as mongoose from 'mongoose';
import { IPassword } from './types';

export const PasswordsSchema = new mongoose.Schema({
  userId: {
    type: String,
    unique: true,
    required: [true, 'Please provide user id'],
  },
  passwords: {
    type: [String],
  },
});

export const Password = mongoose.model<IPassword>('passwords', PasswordsSchema);
