export class NoSavedData extends Error {
  constructor() {
    super('NoSavedData');
    this.message = 'User has no saved data';
    this.name = 'NoSavedData';
  }
}

export class NoSelectedPassword extends Error {
  constructor() {
    super('NoSelectedPassword');
    this.message = 'Selected password is not saved';
    this.name = 'NoSelectedPassword';
  }
}

export class DataAlreadyExists extends Error {
  constructor(name: string) {
    super('DataAlreadyExists');
    this.message = `Selected password ${name} already exists`;
    this.name = 'DataAlreadyExists';
  }
}
