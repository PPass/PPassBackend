import { DataAlreadyExists, NoSavedData, NoSelectedPassword } from './errors';
import { Rooster } from './rooster';
import { UserData } from '../../types';
import { TokenInvalid } from '../../errorsHandling/errors';
import { PasswordsData } from './types';
import { Password } from './model';

export class Controller {
  rooster = new Rooster();

  async getOne(name: string, locals: UserData): Promise<string> {
    if (!locals.id) throw new TokenInvalid();

    const data = await this.rooster.findOneById(locals.id);
    if (!data) throw new NoSavedData();

    const password = data.passwords.find((pass) => pass.name == name);
    if (!password) throw new NoSelectedPassword();
    return password.password;
  }

  async getAll(locals: UserData): Promise<PasswordsData[]> {
    if (!locals.id) throw new TokenInvalid();

    const data = await this.rooster.findOneById(locals.id);
    if (!data) throw new NoSavedData();
    return data.passwords;
  }

  async add(replace: boolean, newPassword: PasswordsData[], locals: UserData): Promise<void> {
    if (!locals.id) throw new TokenInvalid();

    const data = await this.rooster.findOneById(locals.id);
    if (!data) {
      await this.rooster.saveData({ userId: locals.id, passwords: newPassword });
      return;
    }
    let alreadyExists: string | undefined = undefined;
    data.passwords.forEach((pass) => {
      const exists = newPassword.find((data) => data.name == pass.name);
      if (exists) alreadyExists = pass.name;
    });
    if (alreadyExists != undefined && !replace) throw new DataAlreadyExists(alreadyExists);

    data.passwords.concat(newPassword);
    await this.rooster.replace(Password.hydrate(data));
  }

  async removeOne(password: string, locals: UserData): Promise<void> {
    if (!locals.id) throw new TokenInvalid();

    const data = await this.rooster.findOneById(locals.id);
    if (!data) throw new NoSavedData();

    const saved = data.passwords.find((pass) => pass.name == password);
    if (!saved) throw new NoSelectedPassword();

    const filtered = data.passwords.filter((pass) => pass.name != password);
    await this.rooster.replace(Password.hydrate({ ...data, passwords: filtered }));
  }

  async removeAll(locals: UserData): Promise<void> {
    if (!locals.id) throw new TokenInvalid();

    const data = await this.rooster.findOneById(locals.id);
    if (!data) throw new NoSavedData();

    return this.rooster.delete(locals.id);
  }
}
