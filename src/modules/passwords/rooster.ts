import { Password } from './model';
import { IPassword, Passwords } from './types';

export class Rooster {
  async findOneById(id: string): Promise<Passwords | null> {
    return Password.findOne({ _id: id });
  }

  async saveData(data: Passwords): Promise<void> {
    const password = new Password(data);
    await password.save();
  }

  async replace(data: IPassword): Promise<void> {
    await data.save();
  }

  async delete(id: string): Promise<void> {
    await Password.findOneAndRemove({ _id: id });
  }
}
