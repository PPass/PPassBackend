import { Router } from 'express';
import { Controller } from './controller';
import { UserData } from '../../types';

export const router = Router();

router.get('/one', (req, res, next) => {
  const userData = new Controller();

  userData
    .getOne(req.body.name, res.locals as UserData)
    .then((data) => res.send(data))
    .catch((err) => next(err));
});

router.get('/all', (_req, res, next) => {
  const userData = new Controller();

  userData
    .getAll(res.locals as UserData)
    .then((data) => res.send(data))
    .catch((err) => next(err));
});

router.put('/', (req, res, next) => {
  const userData = new Controller();

  userData
    .add(req.body.replace, req.body.passwords, res.locals as UserData)
    .then(() => res.status(200))
    .catch((err) => next(err));
});

router.delete('/one', (req, res, next) => {
  const userData = new Controller();

  userData
    .removeOne(req.body.password, res.locals as UserData)
    .then(() => res.status(200))
    .catch((err) => next(err));
});

router.delete('/all', (_req, res, next) => {
  const userData = new Controller();

  userData
    .removeAll(res.locals as UserData)
    .then(() => res.status(200))
    .catch((err) => next(err));
});
