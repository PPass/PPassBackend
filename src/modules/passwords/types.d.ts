import mongoose from 'mongoose';

export interface IPassword extends mongoose.Document {
  userId: string;
  passwords: PasswordsData[];
}

export interface Passwords {
  userId: string;
  passwords: PasswordsData[];
}

export interface PasswordsData {
  name: string;
  password: string;
}
