import cookieParser from 'cookie-parser';
import cors from 'cors';
import express from 'express';
import { validateToken } from '../utils/controller';

export const genericMiddleware = express();

genericMiddleware.use(express.json());
genericMiddleware.use(cookieParser());
genericMiddleware.use(
  cors({
    origin: '*',
    credentials: true,
  }),
);

genericMiddleware.use((req, res, next) => {
  res.header('Content-Type', 'application/json;charset=UTF-8');
  res.header('Access-Control-Allow-Credentials', 'true');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');

  validateToken(req.cookies, res);

  next();
});
